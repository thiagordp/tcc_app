package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.activities.StatusActivity;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by pc on 25/10/2017.
 */

public class AsyncGetFridgeStatus extends AsyncTask<String, Void, JSONObject> {
    private Context context;
    private View view;
    private int option;

    private final String TAG = "ASYNC_GET_FRIDGE_STATUS";

    public AsyncGetFridgeStatus(Context context, View view, Integer option) {
        this.context = context;
        this.view = view;
        this.option = option;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        if (params.length < 1) {
            Log.e(TAG, "Número de parâmetros insuficiente");

            return null;
        }

        String strURL = params[0];

        Log.d(TAG, "URL:\t" + strURL);

        try {

            URL url = new URL(strURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            Log.d(TAG, "Verificação da resposta");

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String response = Util.convertStreamToString(httpURLConnection.getInputStream());

                Log.d(TAG, "Response:\t\t" + response);
                JSONObject json = new JSONObject(response);
                Log.d(TAG, "JSON\t" + json.toString());

                return json;
            } else {
                Log.e(TAG, "Erro ao conectar com o servidor. Código " + httpURLConnection.getResponseCode());
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if (jsonObject == null) {
            return;
        }

        try {


            Integer status = jsonObject.getInt(Constants.DOOR_STATUS_KEY);


            Toast.makeText(context, "Status: " + status, Toast.LENGTH_LONG).show();
            if (option == 1) {
                TextView tvStatus = (TextView) view.findViewById(R.id.tvStatusFridge);

                if (status.equals(Constants.DOOR_CLOSED)) {
                    tvStatus.setText("Porta Fechada");
                } else if (status.equals(Constants.DOOR_OPENED)) {
                    tvStatus.setText("Porta Aberta");
                }
            } else {
                if (option == 2) {
                    Log.d(TAG, "Notificação: " + status);
                    if (status.equals(Constants.DOOR_OPENED)) {
                        // prepare intent which is triggered if the
                        // notification is selected
                        Intent intent = new Intent(context, StatusActivity.class);
                        // use System.currentTimeMillis() to have a unique ID for the pending intent
                        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

                        // build notification
                        // the addAction re-use the same intent to keep the example short
                        Notification n = new Notification.Builder(context)
                                .setContentTitle("Alerta da geladeira")
                                .setContentText("Você deixou a porta da geladeira aberta por muito tempo. Que tal fechá-la?")
                                .setSmallIcon(R.drawable.image_not_available)
                                .setContentIntent(pIntent)
                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                .setAutoCancel(true).build();

                        NotificationManager notificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                        notificationManager.notify(0, n);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
