package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.ufsc.enc.tcc.app.aninterface.activities.RecipeVisualActivity;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pc on 17/10/2017.
 */

public class AsyncGetRecipeContent extends AsyncTask<String, Void, JSONObject> {

    private final String TAG = "ASYNC_GET_REC_CONTENT";

    private Context context;
    private View view;
    private JSONObject json;

    public AsyncGetRecipeContent(Context context, View view, JSONObject json) {
        this.context = context;
        this.view = view;
        this.json = json;
    }

    protected JSONObject doInBackground(String... params) {

        for (String strlUrl : params) {
            Log.d(TAG, strlUrl);

            try {
                URL url = new URL(strlUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.RECIPE_DESCRIPTION_KEY, json.getString(Constants.RECIPE_DESCRIPTION_KEY));
                jsonObject.put(Constants.RECIPE_URL_KEY, json.getString(Constants.RECIPE_URL_KEY));

                String str = jsonObject.toString();
                byte[] outputBytes = str.getBytes("UTF-8");
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(outputBytes);

                Log.d(TAG, "Verificação de resposta");

                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String response = Util.convertStreamToString(httpURLConnection.getInputStream());

                    Log.d(TAG, "Response:\t" + response);
                    JSONObject jsonResponse = new JSONObject(response);

                    Log.d(TAG, "JSON:\t" + jsonResponse.toString());

                    Intent intent = new Intent(context, RecipeVisualActivity.class);
                    intent.putExtra("metainfo", jsonResponse.toString());
                    context.startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

    }
}
