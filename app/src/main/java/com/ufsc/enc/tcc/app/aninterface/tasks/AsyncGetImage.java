package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.ufsc.enc.tcc.app.aninterface.R;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by trdp on 12/10/17.
 */

public class AsyncGetImage extends AsyncTask<String, Void, Bitmap> {
    private Context context;
    private View view;
    private final String TAG = "ASYNC_GET_IMAGE";

    public AsyncGetImage(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        if (params.length == 0) {
            return null;
        }

        Bitmap imagem = null;
        try {
            String url = params[0];

            Log.d(TAG, "URL: " + url);

            URL Url = new URL(url);
            Log.d(TAG, "Starting connection");
            HttpURLConnection urlConnection = (HttpURLConnection) Url.openConnection();
            Log.d(TAG, "Getting connection status");

            Log.d(TAG, urlConnection.toString());

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Log.d(TAG, "Decoding bitmap");
                imagem = BitmapFactory.decodeStream(urlConnection.getInputStream());
                Log.d(TAG, "Finished bitmap decoding");
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        return imagem;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        if (bitmap != null) {
            Log.d(TAG, bitmap.toString());
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            imageView.setImageBitmap(bitmap);
        }
    }
}
