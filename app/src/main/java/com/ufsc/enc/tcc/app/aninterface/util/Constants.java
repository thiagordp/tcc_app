package com.ufsc.enc.tcc.app.aninterface.util;

/**
 * Created by trdp on 12/10/17.
 */

public class Constants {

    /**********************************
     * Interaction Basis
     *
     *********************************/
    public static final Integer MAX_INTERACTION = 100;

    public static final Integer FRIDGE_COUNT = 10;

    public static final String ID_FRIDGE_KEY = "id_fridge";

    public static final String PRODUCT_LIST_KEY = "products";

    public static final String TIMESTAMP_KEY = "timestamp";

    public static final String PRODUCT_CODE_KEY = "product_code";

    public static final String EPC_CODES_KEY = "epc_codes";

    public static final Integer TYPE_INTERACTION_RECORD = 1;

    public static final Integer TYPE_INTERACTION_DOOR = 2;

    /***********************************
     * Auxiliary structures
     ***********************************/

    public static final String RECORD_TYPE_OPEN_DOOR_KEY = "open_door";

    public static final String RECORD_TYPE_SETTINGS_KEY = "settings";

    public static final String RECORD_TYPE_PURCHASE_LIST = "purchase_list";

    public static final String RECORD_TYPE_KEY = "record_type";

    public static final String DOOR_STATUS_KEY = "door_status";

    public static final Integer DOOR_CLOSED = 1;

    public static final Integer DOOR_OPENED = 0;

    public static final String MARKET_KEY = "market";

    public static final String HOSTNAME_KEY = "hostname";

    public static final String PORT_KEY = "port";

    public static final String PATH_NAME_KEY = "path_name";

    public static final String RESOURCES_KEY = "resources";

    public static final String RESOURCE_PURCHASE_KEY = "purschase";

    public static final String RESOURCE_AVAILABILITY_KEY = "availability";

    public static final String RESOURCE_SYNC_METAINFO_KEY = "sync_metainfo";

    public static final String PURCHASE_PERIOD_KEY = "purchase_period";

    public static final String PRODUCT_QUANTITY_KEY = "product_quantity";

    public static final String PRODUCT_MININUM_QTT_KEY = "minimum_quantity";

    public static final Integer TYPE_DOOR_STATUS = 1;

    public static final Integer TYPE_SETTINGS_KEY = 2;

    public static final Integer TYPE_PURCHASE_LIST_KEY = 3;

    /****************************************
     *   Metainfo basis
     ****************************************/

    public static final String PRODUCT_DESCRIPTION_KEY = "product_description";

    public static final String PRODUCT_MANUFACTURER_KEY = "product_manufacturer";

    public static final String PRODUCT_EXPIRED_TIME_KEY = "product_expired_time";

    public static final String META_INFO_TYPE_KEY = "metainfo_type";

    public static final String META_INFO_PRODUCT_KEY = "meta_info_product";

    public static final String META_INFO_RECIPE = "meta_info_recipe";

    public static final String RECIPE_DESCRIPTION_KEY = "recipe_description";

    public static final String RECIPE_STEPS_KEY = "recipe_steps";

    public static final String RECIPE_CODE_KEY = "recipe_code";

    public static final String RECIPE_URL_KEY = "url";

    public static final String PRODUCT_QUANTITY_UNIT_KEY = "product_quantity_unit";

    public static final String PRODUCT_URL_IMG_KEY = "product_url_image";

    public static final String PRODUCT_KCAL_KEY = "kcal";

    public static final String PRODUCT_TOTAL_FAT_KEY = "total_fat";

    public static final String PRODUCT_BARCODE_KEY = "bar_code";

    public static final String PRODUCT_SODIUM_KEY = "sodium";

    public static final String PRODUCT_PROTEIN_KEY = "protein";

    public static final String PRODUCT_REFERENCE_AMMOUNT_KEY = "reference";

    public static final String PRODUCT_NUTRITIONAL_INFORMATION_KEY = "nutritional_info";

    public static final String PRODUCT_WEIGHT_VOLUME_KEY = "weight_volume";

    public static final String PRODUCT_PRICE_KEY = "price";

    public static final String PRODUCT_CLASSIFICATION_KEY = "classification";

    public static final String PRODUCT_CLASS_KEY = "class";

    public static final String EPC_MANUFACTURER_KEY = "manufacturer";

    public static final String EPC_PRODUCT_KEY = "product";

    public static final String EPC_KEY = "epc";

    public static final Integer EPC_HEADER = 0x1C;

    public static final Integer EPC_NUM_BITS = 96;

    public static final Integer EPC_BIT_HEADER = 88;

    public static final Integer EPC_BIT_MANUFACTURER = 60;

    public static final Integer EPC_BIT_PRODUCT = 36;

    public static final String EPC_BIT_MASK_MANUFACTURER = "268435455"; // (2^28) - 1

    public static final String EPC_BIT_MASK_PRODUCT = "16777215"; // (2^24) - 1

    public static final String EPC_BIT_MASK_SERIAL_NUMBER = "68719476735"; // (2^36) - 1

    public static final String PRODUCT_ESPECIFICATION_KEY = "especfication";

    public static final String NOTIFICATION_DOOR_PERIOD_KEY = "notification_period";

    public static final String RECIPE_CLASSIFICATION_KEY = "classification";

    public static final String RECIPE_CLASS_KEY = "class";

    public static final String RECIPE_ESPECIFICATION_KEY = "especification";

    /*****************************************
     *  Recommendation Basis
     ***************************************/

    public static final String RECOMMENDATION_TYPE_KEY = "recommendation_type";

    public static final String RELEVANCE_KEY = "relevance";

    public static final String PRODUCT_REC_NEW = "product_rec_new";

    public static final String PRODUCT_REC_OLD = "product_rec_old";

    public static final String PRODUCT_REC_SIMILAR = "product_rec_similar";

    public static final String RECIPE_REC = "recipe_rec";

    public static final String OK_JSON = "{ \"status\" : \"OK\"}";

    public static final String ERROR_JSON = "{ \"status\" : \"ERROR\"}";

    public static final String STATUS_AVAILABILITY_KEY = "status_av";

    public static final Integer STATUS_AVAILABLE = 1;

    public static final Integer STATUS_UNAVAILABLE = 0;

    public static final String ORIGINAL_PRODUCT = "orig_poduct";

    public static final String ALTERNATIVE_PRODUCT = "alt_product";

    /****************************************
     * Timers
     **************************************/
    public static final Integer DELAY_TIME_TO_START = 10000;

    public static final Integer EXECUTION_TIME_INTERVAL = 15000;

    public static final Integer MINIMUM_HOURS_OUTSIDE = 1;

    public static final Integer MINIMUM_MATCHES_PRODUCT_RECIPE = 1;

    public static final Integer MAX_REC_RECIPE = 5;

    public static final Integer MAX_PRODUCT_TOP = 5;

    public static final String IP_SERVER = "10.0.0.100";//"192.168.0.105";//"192.168.1.211" "192.168.1.202";

    public static final String SERVER_URL = "http://" + IP_SERVER + ":8080/tcc_server";
}
