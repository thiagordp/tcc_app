package com.ufsc.enc.tcc.app.aninterface.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetFridgeStatus;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSIONS = 300;
    private boolean permissionsOfApp = false;
    private String[] permissions = {Manifest.permission.INTERNET};


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                permissionsOfApp = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }

        if (!permissionsOfApp) {
            Log.d("PERMISSION", "Permissão Negada");
            finish();
        } else {
            Log.d("PERMISSION", "Permissão Aceita");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Conteúdo
        CardView cardView1 = (CardView) findViewById(R.id.cv_content);
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ContentActivity.class);
                startActivity(intent);
            }
        });

        // Lista de compras
        CardView cardView2 = (CardView) findViewById(R.id.cv_purchase_list);
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Boa noite2", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), PurchaseListActivity.class);
                startActivity(intent);
            }
        });

        // Receitas
        CardView cardView3 = (CardView) findViewById(R.id.cv_recipe_list);
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RecipeActivity.class);
                startActivity(intent);
            }
        });

        // Estado Geladeira
        CardView cardView4 = (CardView) findViewById(R.id.cv);
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Boa noite4", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
                startActivity(intent);
            }
        });

        // Estado dos Produtos
        CardView cardView5 = (CardView) findViewById(R.id.cv_product_state);
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Boa noite5", Toast.LENGTH_SHORT).show();
            }
        });

        // Configurações
        CardView cardView6 = (CardView) findViewById(R.id.cv_settings);
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Boa noite6", Toast.LENGTH_SHORT).show();
            }
        });

        ActivityCompat.requestPermissions(this, permissions, 300);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                View view = getWindow().getDecorView().getRootView();
                AsyncGetFridgeStatus asyncGetFridgeStatus = new AsyncGetFridgeStatus(getApplicationContext(), view, 2);
                String url = Constants.SERVER_URL + "/open_door?id_fridge=2";
                asyncGetFridgeStatus.execute(url);
            }
        }, 10000, 100000);
    }
}
