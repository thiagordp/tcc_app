package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.activities.MainActivity;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by pc on 24/10/2017.
 */

public class AsyncSendPurchaseList extends AsyncTask<String, Void, JSONObject> {
    private Context context;
    private View view;

    private final String TAG = "ASYNC_SEND_PURCHASE";

    public AsyncSendPurchaseList(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        if (params.length < 2) {
            Log.e(TAG, "Número de parâmetros insuficiente");

            return null;
        }

        String strURL = params[0];
        String strJSON = params[1];

        Log.d(TAG, "URL:\t" + strURL);

        try {
            //  JSONObject jsonProducts = new JSONObject(strJSON);

            URL url = new URL(strURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(15000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            byte[] outBytes = strJSON.getBytes("UTF-8");
            OutputStream os = httpURLConnection.getOutputStream();
            os.write(outBytes);

            Log.d(TAG, "Verificação da resposta");

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                String response = Util.convertStreamToString(httpURLConnection.getInputStream());

                Log.d(TAG, "Response:\t\t" + response);
                JSONObject json = new JSONObject(response);
                Log.d(TAG, "JSON\t" + json.toString());

                return json;
            } else {
                Log.e(TAG, "Erro ao conectar com o servidor. Código " + httpURLConnection.getResponseCode());
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if (jsonObject == null) {
            return;
        }

        Toast.makeText(context, "Compra realizada com sucesso!", Toast.LENGTH_LONG).show();

        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(i);
    }
}
