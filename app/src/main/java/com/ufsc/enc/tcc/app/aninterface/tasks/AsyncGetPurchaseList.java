package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.PurchaseList;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by pc on 19/10/2017.
 */

public class AsyncGetPurchaseList extends AsyncTask<String, Void, JSONObject> {

    private Context context;
    private View view;

    private final String TAG = "ASYNC_PURCHASE";

    public AsyncGetPurchaseList(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        for (String strURL : params) {
            Log.d(TAG, "URL:\t" + strURL);

            try {
                URL url = new URL(strURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                Log.d(TAG, "Verificação da resposta");

                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String response = Util.convertStreamToString(httpURLConnection.getInputStream());

                    Log.d(TAG, "Response:\t\t" + response);
                    JSONObject json = new JSONObject(response);
                    Log.d(TAG, "JSON\t" + json.toString());

                    return json;
                } else {
                    Log.e(TAG, "Erro ao conectar com o servidor. Código " + httpURLConnection.getResponseCode());
                }
            } catch (MalformedURLException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if (jsonObject == null) {
            return;
        }

        try {
            JSONArray jsonArrayProds = jsonObject.getJSONArray(Constants.PRODUCT_LIST_KEY);
            Integer lengthArray = jsonArrayProds.length();


            String[] name = new String[lengthArray];
            String[] typeRecs = new String[lengthArray];
            String[] qtds = new String[lengthArray];
            String[] urls = new String[lengthArray];

            for (int i = 0; i < jsonArrayProds.length(); i++) {
                JSONObject jsonObj = jsonArrayProds.getJSONObject(i);

                name[i] = jsonObj.getString(Constants.PRODUCT_DESCRIPTION_KEY);
                typeRecs[i] = jsonObj.getString(Constants.RECOMMENDATION_TYPE_KEY);

                qtds[i] = jsonObj.getString(Constants.PRODUCT_QUANTITY_KEY);
                urls[i] = jsonObj.getString(Constants.PRODUCT_URL_IMG_KEY);
            }

            PurchaseList purchaseList = new PurchaseList((Activity) this.context, name, urls, qtds, typeRecs);

            ListView listView = (ListView) view.findViewById(R.id.lvPurchase);
            listView.setAdapter(null);
            listView.setAdapter(purchaseList);

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
    }
}
