package com.ufsc.enc.tcc.app.aninterface.util;

/**
 * Created by trdp on 28/09/17.
 */


import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetImage;


/**
 * Created by trdp on 2/7/17.
 */

public class CustomList extends ArrayAdapter<String> {

    private Activity context;

    // Parâmetros que cada item da lista vai ter.
    private String[] barcodes;
    private String[] names;
    private String[] qtds;
    private String[] units;
    private String[] urls;

    public CustomList(Activity context, String[] barcodes, String[] names, String[] qtds, String[] units, String[] urls) {
        super(context, R.layout.list_item, names);

        this.context = context;
        this.barcodes = barcodes;
        this.names = names;
        this.qtds = qtds;
        this.units = units;
        this.urls = urls;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item, null, true);
        TextView tvBarcode = (TextView) rowView.findViewById(R.id.tvBarcode);
        TextView tvName = (TextView) rowView.findViewById(R.id.tvName);
        TextView tvQtt = (TextView) rowView.findViewById(R.id.tvQtt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.image_not_available);

        tvBarcode.setText("");
        tvName.setText("");
        tvQtt.setText("");

        // Tarefa que faz requisição da imagem de representação do produto em segundo plano.
        // AsyncTaskImage taskImage = new AsyncTaskImage(getContext(), rowView);
        //   taskImage.execute(urls[position]);
        try {
            AsyncGetImage taskImage = new AsyncGetImage(getContext(), rowView);
            taskImage.execute(urls[position]);

            tvBarcode.setText(String.valueOf(barcodes[position]));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String name = names[position];
        try {
            name += " - " + units[position];

        } catch (Exception e) {
            e.printStackTrace();
        }
        tvName.setText(name);
        try {
            tvQtt.setText(qtds[position]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }
}
