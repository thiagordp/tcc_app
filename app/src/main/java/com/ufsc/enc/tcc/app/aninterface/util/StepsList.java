package com.ufsc.enc.tcc.app.aninterface.util;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetImage;


/**
 * Created by pc on 17/10/2017.
 */

public class StepsList extends ArrayAdapter<String> {

    private Activity context;
    private String[] steps;

    public StepsList(Activity context, String[] steps) {
        super(context, R.layout.list_steps, steps);

        this.context = context;
        this.steps = steps;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_steps, null, true);
        TextView tvBarcode = (TextView) rowView.findViewById(R.id.tvSteps);
        String step = "• " + steps[position];
        tvBarcode.setText(step);

        return rowView;
    }
}
