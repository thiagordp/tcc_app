package com.ufsc.enc.tcc.app.aninterface.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetContent;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;

public class ContentActivity extends AppCompatActivity {

    private View view;
    private final String TAG = "CONTENT_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        this.view = getWindow().getDecorView().getRootView();
        AsyncGetContent asyncGetContent = new AsyncGetContent(this, this.view);

        String url = Constants.SERVER_URL + "/available_products?id_fridge=2";
        Log.d(TAG, "Executing...");
        asyncGetContent.execute(url);
    }
}

