package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.CustomList;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by trdp on 13/10/17.
 */
public class AsyncGetRecipes extends AsyncTask<String, Void, JSONObject> {

    private final String TAG = "ASYNC_GET_RECIPE";

    private Context context;
    private View view;
    private JSONObject json;

    public AsyncGetRecipes(Context context, View view, JSONObject json) {
        this.context = context;
        this.view = view;
        this.json = json;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        for (String strURL : params) {
            Log.d(TAG, "URL:\t" + strURL);
            try {
                URL url = new URL(strURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                Log.d(TAG, "Verificação da resposta");

                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String response = Util.convertStreamToString(httpURLConnection.getInputStream());

                    Log.d(TAG, "Response:\t\t" + response);
                    JSONObject jsons = new JSONObject(response);

                    Log.d(TAG, "JSON\t" + jsons.toString());

                    return jsons;
                } else {
                    Log.e(TAG, "Erro ao conectar com o servidor. Código " + httpURLConnection.getResponseCode());
                }

            } catch (MalformedURLException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if (jsonObject == null) {
            return;
        }

        try {
            JSONArray jsonProducts = jsonObject.getJSONArray(Constants.PRODUCT_LIST_KEY);

            Integer lenArray = jsonProducts.length();
            Log.d(TAG, "Receitas:\t" + lenArray);
            Log.d(TAG, "ARRAY:\t" + jsonProducts.toString());

            HashMap<String, String> tmp = new HashMap<>();

            if (jsonProducts.length() == 0) {
                Toast.makeText(context, "Não foi possível encontrar nenhuma receita", Toast.LENGTH_SHORT).show();
                return;
            }

            for (int i = 0; i < lenArray; i++) {

                JSONObject json = jsonProducts.getJSONObject(i);
                Log.d(TAG, "JSON " + i + ":\t" + json.toString());

                try {
                    tmp.put(json.getString(Constants.RECIPE_DESCRIPTION_KEY), json.getString(Constants.RECIPE_URL_KEY));
                } catch (Exception e) {
                    //
                }
            }

            int lenList = tmp.size();
            String[] nome = new String[lenList];
            String[] urls = new String[lenList];

            int count = 0;

            for (String key : tmp.keySet()) {

                nome[count] = key;
                urls[count] = tmp.get(key);
                count++;
            }

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < tmp.size(); i++) {
                JSONObject json = new JSONObject();
                json.put(Constants.RECIPE_DESCRIPTION_KEY, nome[i]);
                json.put(Constants.RECIPE_URL_KEY, urls[i]);
                json.put("position", i);

                jsonArray.put(json);
            }

            this.json.put(Constants.PRODUCT_LIST_KEY, jsonArray);
            Log.d(TAG, "JSON_LIST:\t" + json.toString());

            Log.d(TAG, "ARRAYNOME:\t" + nome);
            Log.d(TAG, "ARRAYURL:\t" + urls);

            ListView listView;
            listView = (ListView) view.findViewById(R.id.lvRecipes);
            CustomList customList = new CustomList((Activity) context, null, nome, null, null, urls);

            listView.setAdapter(null);
            listView.setAdapter(customList);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            e.printStackTrace();
        }
    }
}
