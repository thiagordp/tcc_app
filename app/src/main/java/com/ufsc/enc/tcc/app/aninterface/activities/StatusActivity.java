package com.ufsc.enc.tcc.app.aninterface.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetFridgeStatus;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;

public class StatusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        View view = getWindow().getDecorView().getRootView();

        String url = Constants.SERVER_URL + "/open_door?id_fridge=2";

        AsyncGetFridgeStatus asyncGetFridgeStatus = new AsyncGetFridgeStatus(getBaseContext(), view, 1);
        asyncGetFridgeStatus.execute(url);
    }
}
