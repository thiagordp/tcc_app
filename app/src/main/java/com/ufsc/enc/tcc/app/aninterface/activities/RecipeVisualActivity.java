package com.ufsc.enc.tcc.app.aninterface.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetImage;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.IngredientsList;
import com.ufsc.enc.tcc.app.aninterface.util.StepsList;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.List;

public class RecipeVisualActivity extends AppCompatActivity {

    private final String TAG = "REC_VISUAL_ACTIVITY";
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_visual);

        this.view = getWindow().getDecorView().getRootView();

        String metainfo = getIntent().getStringExtra("metainfo");
        Log.d(TAG, "METAINFO:\t" + metainfo);


        try {
            JSONObject jsonObject = new JSONObject(metainfo);
            String url = jsonObject.getString(Constants.RECIPE_URL_KEY);
            AsyncGetImage asyncGetImage = new AsyncGetImage(getApplicationContext(), view);
            asyncGetImage.execute(url);


            JSONArray jsonSteps = jsonObject.getJSONArray(Constants.RECIPE_STEPS_KEY);
            String[] steps = new String[jsonSteps.length()];

            for (int i = 0; i < jsonSteps.length(); i++) {
                steps[i] = jsonSteps.getString(i);
            }

            StepsList stepsList = new StepsList(this, steps);
            ListView lvSteps = (ListView) findViewById(R.id.lvSteps);
            TextView tvTitle = (TextView) findViewById(R.id.tvTitle);

            tvTitle.setText(jsonObject.getString(Constants.RECIPE_DESCRIPTION_KEY));

            Log.d(TAG, "STEPS:\t" + jsonSteps.toString());

            lvSteps.setAdapter(null);
            lvSteps.setAdapter(stepsList);
            Util.setListViewHeightBasedOnItems(lvSteps);

            JSONArray jsonProducts = jsonObject.getJSONArray(Constants.PRODUCT_LIST_KEY);


            String[] description = new String[jsonProducts.length()];
            String[] quantity = new String[jsonProducts.length()];

            for (int i = 0; i < jsonProducts.length(); i++) {
                JSONObject json = jsonProducts.getJSONObject(i);
                Log.d(TAG, "INGR:\t" + json.toString());
                description[i] = json.getString(Constants.PRODUCT_BARCODE_KEY);
                quantity[i] = json.getString(Constants.PRODUCT_QUANTITY_KEY);
                quantity[i] += " " + json.getString(Constants.PRODUCT_QUANTITY_UNIT_KEY);
            }

            IngredientsList ingredientsList = new IngredientsList(this, description, quantity);
            ListView lvIngri = (ListView) findViewById(R.id.lvIngredients);
            lvIngri.setAdapter(null);
            lvIngri.setAdapter(ingredientsList);
            Util.setListViewHeightBasedOnItems(lvIngri);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
