package com.ufsc.enc.tcc.app.aninterface.util;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetImage;

import org.w3c.dom.Text;

/**
 * Created by pc on 19/10/2017.
 */

public class PurchaseList extends ArrayAdapter<String> {

    private Activity context;

    private String[] descriptions;
    private String[] urls;
    private String[] quantitys;
    private String[] typeRecs;

    public PurchaseList(Activity context, String[] descriptions, String[] urls, String[] quantitys, String[] typeRecs) {
        super(context, R.layout.list_purchase_item, descriptions);

        this.context = context;
        this.descriptions = descriptions;
        this.urls = urls;
        this.quantitys = quantitys;
        this.typeRecs = typeRecs;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_purchase_item, null, true);

        TextView tvName = (TextView) rowView.findViewById(R.id.tvName);
        TextView tvQuantity = (TextView) rowView.findViewById(R.id.tvQtt);
        TextView tvType = (TextView) rowView.findViewById(R.id.tvType);

        tvName.setText(this.descriptions[position]);
        tvQuantity.setText(this.quantitys[position]);
        tvType.setText(this.typeRecs[position]);

        AsyncGetImage asyncGetImage = new AsyncGetImage(this.context, rowView);
        asyncGetImage.execute(urls[position]);

        return rowView;
    }
}
