package com.ufsc.enc.tcc.app.aninterface.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetPurchaseList;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncSendPurchaseList;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

public class PurchaseListActivity extends AppCompatActivity {

    private View view;
    private final String TAG = "PURCHASE_LIST_ACT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_list);

        this.view = getWindow().getDecorView().getRootView();

        String url = Constants.SERVER_URL + "/purchase_recommender?id_fridge=2";
        AsyncGetPurchaseList asyncGetPurchaseList = new AsyncGetPurchaseList(this, this.view);
        asyncGetPurchaseList.execute(url);

        Button btnSend = (Button) findViewById(R.id.btnSend);
        final ListView lvProduct = (ListView) findViewById(R.id.lvPurchase);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = getWindow().getDecorView().getRootView();
                AsyncSendPurchaseList asyncSendPurchaseList = new AsyncSendPurchaseList(getBaseContext(), v);
                String url = Constants.SERVER_URL + "/purchase_list";

                HashMap<String, Double> prodArray = new HashMap<String, Double>();

                try {
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();

                    // TODO: Analisar cada selecionado

                    JSONObject jsonObj;

                    /* = new JSONObject();
                    jsonObj.put(Constants.PRODUCT_BARCODE_KEY, "987654");
                    jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, 3);
                    jsonArray.put(jsonObj);

                    jsonObj = new JSONObject();

                    jsonObj.put(Constants.PRODUCT_BARCODE_KEY, "456789");
                    jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, 5);
                    jsonArray.put(jsonObj);*/

                    boolean existSelected = false;


                    /////////////////////////////////////////////////
                    for (int i = 0; i < lvProduct.getCount(); i++) {
                        View listItemView = Util.getViewByPosition(i, lvProduct);

                        CheckBox checkBox = (CheckBox) listItemView.findViewById(R.id.cbPurchase);

                        if (checkBox != null && checkBox.isChecked()) {
                            existSelected = true;

                            TextView tvName = (TextView) listItemView.findViewById(R.id.tvName);
                            TextView tvQtt = (TextView) listItemView.findViewById(R.id.tvQtt);


                            String prodName = tvName.getText().toString();
                            String prodQtt = tvQtt.getText().toString();
                            Log.d(TAG, "NAME:\t" + prodName + "\t\tQtd:\t" + prodQtt);

                            prodArray.put(prodName, Double.valueOf(prodQtt));
                        }
                    }

                    if (!existSelected) {
                        Toast.makeText(getBaseContext(), "Selecione pelo menos um produto!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    for (String key : prodArray.keySet()) {
                        jsonObj = new JSONObject();
                        jsonObj.put(Constants.PRODUCT_DESCRIPTION_KEY, key);
                        jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, prodArray.get(key));

                        jsonArray.put(jsonObj);
                    }

                    jsonObject.put(Constants.ID_FRIDGE_KEY, 1);
                    jsonObject.put(Constants.PRODUCT_LIST_KEY, jsonArray);

                    String jsonStr = jsonObject.toString();

                    Log.d(TAG, "JSON:\t" + jsonStr);
                    asyncSendPurchaseList.execute(url, jsonStr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        /*
        String[] name = new String[2];
        String[] urls = new String[2];
        String[] qtds = new String[2];
        String[] typeRecs = new String[2];

        name[0] = "ABCDEF";
        name[1] = "prato";

        urls[0] = "http://www.tirol.com.br/media/cache/product_vertical/files/product/1506d7c617f17e34ce63d60adcacb249.png";
        urls[1] = "http://www.hippo.com.br/uploads/Produto/97451/Default/grande_15ed59e88f33a766b95897284ecd37ed.jpeg";

        qtds[0] = "10";
        qtds[1] = "5";

        typeRecs[0] = "Novo!";
        typeRecs[1] = "Reposição";

        PurchaseList purchaseList = new PurchaseList(this, name, urls, qtds, typeRecs);
        ListView listView = (ListView) view.findViewById(R.id.lvPurchase);
        listView.setAdapter(null);
        listView.setAdapter(purchaseList);*/
    }
}
