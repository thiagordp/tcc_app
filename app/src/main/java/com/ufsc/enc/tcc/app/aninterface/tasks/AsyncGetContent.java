package com.ufsc.enc.tcc.app.aninterface.tasks;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;
import com.ufsc.enc.tcc.app.aninterface.util.CustomList;
import com.ufsc.enc.tcc.app.aninterface.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by trdp on 12/10/17.
 */

/**
 * Param: url
 * Progress: void
 * Result: Lista de produtos
 */
public class AsyncGetContent extends AsyncTask<String, Void, JSONObject> {

    private final String TAG = "ASYNC_GET_CONTENT";

    private Context context;
    private View view;

    public AsyncGetContent(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    @Override
    protected JSONObject doInBackground(String... params) {

        for (String strURL : params) {
            Log.d(TAG, "URL:\t" + strURL);

            try {
                URL url = new URL(strURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                Log.d(TAG, "Verificação da resposta");

                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String response = Util.convertStreamToString(httpURLConnection.getInputStream());

                    Log.d(TAG, "Response:\t\t" + response);
                    JSONObject json = new JSONObject(response);
                    Log.d(TAG, "JSON\t" + json.toString());

                    return json;
                } else {
                    Log.e(TAG, "Erro ao conectar com o servidor. Código " + httpURLConnection.getResponseCode());
                }

            } catch (MalformedURLException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if (jsonObject == null) {
            return;
        }
        try {
            JSONArray jsonArrayProds = jsonObject.getJSONArray(Constants.PRODUCT_LIST_KEY);
            Integer lengthArray = jsonArrayProds.length();

            if (lengthArray == 0) {
                Toast.makeText(context, "Nenhum produto foi encontrado", Toast.LENGTH_SHORT).show();
                return;
            }

            String[] barcodes = new String[lengthArray];
            String[] names = new String[lengthArray];
            String[] manufacturers = new String[lengthArray];
            String[] qtds = new String[lengthArray];
            String[] urls = new String[lengthArray];

            for (int i = 0; i < jsonArrayProds.length(); i++) {

                JSONObject jsonObj = jsonArrayProds.getJSONObject(i);

                barcodes[i] = jsonObj.getString(Constants.PRODUCT_BARCODE_KEY);
                names[i] = jsonObj.getString(Constants.PRODUCT_DESCRIPTION_KEY);
                manufacturers[i] = jsonObj.getString(Constants.PRODUCT_MANUFACTURER_KEY);
                qtds[i] = jsonObj.getString(Constants.PRODUCT_QUANTITY_KEY);
                urls[i] = jsonObj.getString(Constants.PRODUCT_URL_IMG_KEY);
            }

            ListView listView;
            listView = (ListView) view.findViewById(R.id.lvContent);
            CustomList customList = new CustomList((Activity) context, barcodes, names, qtds, manufacturers, urls);
            listView.setAdapter(null);
            listView.setAdapter(customList);

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
    }
}
