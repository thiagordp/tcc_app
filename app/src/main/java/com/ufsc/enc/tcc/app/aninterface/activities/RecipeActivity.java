package com.ufsc.enc.tcc.app.aninterface.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetRecipeContent;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetRecipes;
import com.ufsc.enc.tcc.app.aninterface.util.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

public class RecipeActivity extends AppCompatActivity {
    private View _view;
    private final String TAG = "RECIPE_ACTIVITY";
    private JSONObject json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        this.json = new JSONObject();
        this._view = getWindow().getDecorView().getRootView();

        Log.d(TAG, "Starting recipe listing");
        ListView listView = (ListView) findViewById(R.id.lvRecipes);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, String.valueOf(position));

                //  Usar json object aqui
                TextView tvName = view.findViewById(R.id.tvName);
                Toast.makeText(RecipeActivity.this, String.valueOf(position) + "\t-\t" + tvName.getText().toString(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "| JSON:\t" + json);

                try {
                    JSONArray jsonArray = json.getJSONArray(Constants.PRODUCT_LIST_KEY);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int pos = jsonObject.getInt("position");

                            if (pos == position) {

                                AsyncGetRecipeContent asyncGetRecipeContent = new AsyncGetRecipeContent(getApplicationContext(), view, jsonObject);

                                String _url = Constants.SERVER_URL + "/recipe_info";
                                asyncGetRecipeContent.execute(_url);

                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        /*
        * Executar async
        */
        String url = Constants.SERVER_URL + "/recipe_recommender?id_fridge=2";
        Log.d(TAG, "Executing...");

        AsyncGetRecipes asyncGetRecipes = new AsyncGetRecipes(this, this._view, json);
        asyncGetRecipes.execute(url);
/*
        String[] nullStr = new String[2];
        String[] nomes = new String[2];
        nomes[0] = "Trança de leite condensado";
        nomes[1] = "Bife a milanesa";

        String[] urls = new String[2];
        urls[0] = "https://abrilmdemulher.files.wordpress.com/2017/04/receita-torta-marmore-de-leite-condensado163.jpeg?quality=90&strip=info&w=654";
        urls[1] = "http://www.tudonapanela.com.br/media/k2/items/cache/de2df791682f079f8397226a3ff38bc7_L.jpg";

        CustomList customList = new CustomList(this, null, nomes, null, null, urls);
        listView.setAdapter(null);
        listView.setAdapter(customList);*/
    }
}
