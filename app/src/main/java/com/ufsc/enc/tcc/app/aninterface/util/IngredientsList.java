package com.ufsc.enc.tcc.app.aninterface.util;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ufsc.enc.tcc.app.aninterface.R;
import com.ufsc.enc.tcc.app.aninterface.tasks.AsyncGetImage;

/**
 * Created by pc on 17/10/2017.
 */

public class IngredientsList extends ArrayAdapter<String> {
    private Activity context;

    // Parâmetros que cada item da lista vai ter.
    private String[] description;
    private String[] quantity;

    public IngredientsList(Activity context, String[] description, String[] quantity) {
        super(context, R.layout.list_ingredient, description);

        this.context = context;
        this.description = description;
        this.quantity = quantity;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_ingredient, null, true);
        TextView tvDescription = (TextView) rowView.findViewById(R.id.tvIngredientName);
        TextView tvQtt = (TextView) rowView.findViewById(R.id.tvQuantity);

        // Tarefa que faz requisição da imagem de representação do produto em segundo plano.
        // AsyncTaskImage taskImage = new AsyncTaskImage(getContext(), rowView);
        //   taskImage.execute(urls[position]);

        tvDescription.setText(description[position]);
        tvQtt.setText(quantity[position]);

        return rowView;
    }
}
